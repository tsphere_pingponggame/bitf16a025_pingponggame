var leftPaddle;
var rightPaddle;
var ball;
var leftUp=false;
var leftDown=false;
var rightUp=false;
var rightDown=false;
var left_player_scores = 0;
var right_player_scores = 0;
var turnL=false;
var turnR=true;
var pause = false;

var iterator = 0;
var isGamePlaying = false;
var shouldRestart = false;
var replayinterval;

window.addEventListener("keydown", function(e) {
    // space and arrow keys
    if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
        e.preventDefault();
    }
}, false);

function playGame(e){
    if(e.keyCode == 13 && !isGamePlaying){
        isGamePlaying = true;
        window.clearInterval(myGameArea.showStartScreenInterval);
        left_player_scores = 0;
        right_player_scores = 0;
        //start playing game
        myGameArea.play();
        leftPaddle=new component(15,150,"blue",50,280);
        rightPaddle=new component(15,150,"blue",835,280);
        ball=new ballComponent(15,"red",450,350);
    }
}

function replayGame(e){
    if(e.keyCode == 13 && shouldRestart){
        window.location.reload();
    }
}

function startGame(){
    myGameArea.showStartScreen();
    ball=new ballComponent(15,"red",450,350);
}
var myGameArea = {
    canvas : document.createElement("canvas"),
    /********************* start screen function ************/
    showStartScreen: function(){
        //Setting up canvas
        this.canvas.width=900;
        this.canvas.height=640;
        this.canvas.style.backgroundColor='black';
        this.canvas.style.boxShadow='0 0 10px #999';
        this.context=this.canvas.getContext("2d");
        document.addEventListener("keyup",playGame,true);
        //Adding in HTML        
        document.body.insertBefore(this.canvas,document.body.childNodes[0]);
        //update after every 20ms
        this.showStartScreenInterval = setInterval(updateStartScreen, 15);

    },
    /********************* play function ********************/
    play : function () {
        //Setting up canvas
        this.canvas.width=900;
        this.canvas.height=640;
        this.canvas.style.backgroundColor='black';
        document.addEventListener("keydown",movePaddle,true);
        document.addEventListener("keyup",stopPaddles,true);
        this.context=this.canvas.getContext("2d");        
        //Adding in HTML        
        document.body.insertBefore(this.canvas,document.body.childNodes[0]);
        //update after every 20ms
        this.playInterval = setInterval(updatePlayScreen, 15);
        this.winnerObserverInterval = setInterval(observeWinner, 20);

    },
    /********************* score function *********************************/
    score:function (right_player_scores, left_player_scores){
        ctx = this.context;
        ctx.font="30px Comic Sans MS";
        ctx.fillStyle = "blue";
        ctx.textAlign = "center";
        ctx.fillText(left_player_scores, this.canvas.width/2 - 50, 50);
        ctx.fillText(right_player_scores, this.canvas.width/2 + 50, 50);
    },
    clearPlayScreen:function(){
        this.context.clearRect(0,0,this.canvas.width,this.canvas.height);
        //Creating verticle line in center
        ctx=this.context;
        ctx.moveTo(this.canvas.width/2, 10);
        ctx.lineTo(this.canvas.width/2, this.canvas.height-10);
        ctx.strokeStyle='blue';
        ctx.lineWidth=3;
        ctx.shadowColor='blue';
        ctx.shadowBlur=5;
        ctx.stroke(); 
        //Creating border
        ctx.moveTo(10, 10);
        ctx.lineTo(this.canvas.width-10, 10);
        ctx.lineTo(this.canvas.width-10, this.canvas.height-10);        
        ctx.lineTo(10, this.canvas.height-10);        
        ctx.lineTo(10, 10);
        ctx.closePath();
        ctx.stroke();
    },
    clearStartScreen:function(){
        this.context.clearRect(0,0,this.canvas.width,this.canvas.height);

        //Creating border
        ctx=this.context;
        ctx.strokeStyle='blue';
        ctx.lineWidth=3;
        ctx.shadowColor='blue';
        ctx.shadowBlur=5;
        ctx.stroke(); 
        ctx.moveTo(10, 10);
        ctx.lineTo(this.canvas.width-10, 10);
        ctx.lineTo(this.canvas.width-10, this.canvas.height-10);        
        ctx.lineTo(10, this.canvas.height-10);        
        ctx.lineTo(10, 10);
        ctx.closePath();
        ctx.stroke();

        //Writing text
        ctx.font="75px Comic Sans MS";
        ctx.fillStyle = "blue";
        ctx.textAlign = "center";
        ctx.fillText("Ping Pong", this.canvas.width/2, this.canvas.height/2);

        if(iterator++ % 2 == 0)
            promptMessage("<press enter key to play>", "blue");
        else
            promptMessage("<press enter key to play>", "white");
    }

}
/**************************** <prompt message> ******************/
function promptMessage(message,color){
    ctx = myGameArea.canvas.getContext("2d");
    //Writing text
    ctx.font="20px Comic Sans MS";
    ctx.fillStyle = color;
    ctx.textAlign = "center";
    ctx.fillText(message, myGameArea.canvas.width/2, myGameArea.canvas.height/2 + 70);
}
/**************************** </prompt message> ******************/
/**************************** <left winner message> ******************/
function leftWinnerMessage(color){
    ctx = myGameArea.context;
    //Writing text
    ctx.font="50px Comic Sans MS";
    ctx.fillStyle = color;
    ctx.textAlign = "center";
    ctx.fillText("YOU WON!", myGameArea.canvas.width/2 - 150, myGameArea.canvas.height/2);
}
/**************************** </left winner message> ******************/
/**************************** <right winner message> ******************/
function rightWinnerMessage(color){
    ctx = myGameArea.context;
    //Writing text
    ctx.font="50px Comic Sans MS";
    ctx.fillStyle = color;
    ctx.textAlign = "center";
    ctx.fillText("YOU WON!", myGameArea.canvas.width/2 + 150, myGameArea.canvas.height/2);
}
/**************************** </right winner message> ******************/
function movePaddle(e){    
    //W---Left Paddle Up
    if(e.keyCode==87){
        leftUp=true;
        leftDown=false;
    }
    //S---Left Paddle Down
    if(e.keyCode==83){
        leftUp=false;
        leftDown=true;
    }
    //ArrowUp---Right Paddle Up
    if(e.keyCode==38){
        rightUp=true;
        rightDown=false;
    }
    //ArrowDown---Right Paddle Down
    if(e.keyCode==40){
        rightUp=false;
        rightDown=true;        
    }
    if(e.keyCode == 32)
    {
        pause = !pause;
    }
}

function updatePostions(){
    if(leftUp==true){
        if(leftPaddle.y>=0){
            leftPaddle.y-=6;    
        }
    }
    if(leftDown==true){
        if(leftPaddle.y<=myGameArea.canvas.height-leftPaddle.height){
            leftPaddle.y+=6;        
        }
    }
    if(rightUp==true){
        if(rightPaddle.y>=0){
            rightPaddle.y-=6;    
        }
    }
    if(rightDown==true){
        if(rightPaddle.y<=myGameArea.canvas.height-rightPaddle.height){
            rightPaddle.y+=6;        
        }
    }    
}

function updatePlayScreen(){
    if(!pause)
    {
        myGameArea.clearPlayScreen();   
        myGameArea.score(left_player_scores,right_player_scores);
        updatePostions();
        updateBallPosition();
        leftPaddle.update();
        rightPaddle.update();
        ball.update();
    }
}

/***************************************************************************/
/************************** <check if won> *********************************/
/***************************************************************************/
function isWon(){
    var winnigScore = 10;
    if(left_player_scores > winnigScore){
        return "RIGHT";
    }else if(right_player_scores > winnigScore){
        return "LEFT";
    }else{
        return "NO";
    }
}
/***************************************************************************/
/************************** </check if won> *********************************/
/***************************************************************************/

/***************************************************************************/
/************************** <check if won> *********************************/
/***************************************************************************/
function observeWinner(){
    if(isWon() == "LEFT"){
        shouldRestart = true;
        leftWinnerMessage("green");
    }else if(isWon() == "RIGHT"){
        shouldRestart = true;
        rightWinnerMessage("green");
    }
    
    if(shouldRestart){
        window.clearInterval(myGameArea.playInterval);
        promptMessage("<press enter to go back>", "red");
        isGamePlaying = false;
        window.clearInterval(myGameArea.winnerObserverInterval);
        document.addEventListener("keyup",replayGame,true);
    }
}
/***************************************************************************/
/************************** </check if won> *********************************/
/***************************************************************************/

/***************************************************************************/
/************************** <update start screen> *************************/
/***************************************************************************/
function updateStartScreen(){
    myGameArea.clearStartScreen();
    ball.wander();
}
/***************************************************************************/
/************************** </update start screen> *************************/
/***************************************************************************/
function stopPaddles(e){
    if(e.keyCode==87||e.keyCode==83){
        leftUp=false;
        leftDown=false;
    }
    if(e.keyCode==38||e.keyCode==40){
        rightUp=false;
        rightDown=false;    
    }
}

function component(width, height, color, x, y) {
    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;
    this.update = function(){
        ctx = myGameArea.context;
        ctx.fillStyle = color;
        ctx.shadowBlur=10;
        ctx.shadowColor=color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    }
}

function ballComponent(radius,color,x,y){
    this.centerX = x;
    this.centerY = y;
    this.radius = radius;
    this.color=color;
    this.speed = 5;
    this.velocityX = 5;
    this.velocityY = 5;

    this.update=function(){
        ctx=myGameArea.context;
        ctx.beginPath();
        ctx.arc(this.centerX, this.centerY, 10, 0, Math.PI*2, true); 
        ctx.closePath();
        ctx.shadowBlur=5;
        ctx.shadowColor='white';
        ctx.fillStyle=this.color;
        ctx.fill();
        ctx.lineWidth=2;
        ctx.strokeStyle='white';
        ctx.stroke();
    }

    this.wander=function(){
        this.update();

        ball.centerX += ball.velocityX;
        ball.centerY += ball.velocityY;

        if(ball.centerY + ball.radius >= myGameArea.canvas.height || ball.centerY - ball.radius <= 0)
        {
            ball.velocityY = -ball.velocityY;
        }
        else if(ball.centerX + ball.radius >= myGameArea.canvas.width)
        {
            right_player_scores +=1;
            ball.velocityX=-ball.velocityX;

        }
        else if(ball.centerX - ball.radius <= 0)
        {
            left_player_scores +=1;
            ball.velocityX=-ball.velocityX;
        }
    }
}

function updateBallPosition(){
    ball.centerX += ball.velocityX;
    ball.centerY += ball.velocityY;

    if(ball.centerY + ball.radius >= myGameArea.canvas.height || ball.centerY - ball.radius <= 0)
    {
        ball.velocityY = -ball.velocityY;
    }
    else if(ball.centerX + ball.radius >= myGameArea.canvas.width)
    {
        ball.centerX = 450;
        ball.centerY = 350;
        right_player_scores +=1;
        turnL=true;
        turnR=false;
        ball.velocityX=-ball.velocityX;

        pause = true;
    }
    else if(ball.centerX - ball.radius <= 0)
    {
        ball.centerX = 450;
        ball.centerY = 350;
        left_player_scores +=1;
        turnL=false;
        turnR=true;
        ball.velocityX=-ball.velocityX;
        pause = true;   
    }
    //Left Paddle Horizontal Collision
    else if(ball.centerX-ball.radius>=leftPaddle.x&&ball.centerX+ball.radius<=leftPaddle.x+leftPaddle.width&&
            ball.centerY+ball.radius>=leftPaddle.y&&ball.centerY-ball.radius<=leftPaddle.y+leftPaddle.height){
        ball.velocityY=-ball.velocityY;
    }
    //Left Paddle Veriticle Collision
    else if((ball.centerY)>=leftPaddle.y&&
            (ball.centerY)<=(leftPaddle.y+leftPaddle.height)&&
            ((ball.centerX-ball.radius)<=(leftPaddle.x+leftPaddle.width))){
        if(turnL){
            ball.velocityX = -ball.velocityX;
            turnL=false;
            turnR=true;
        }
    }
    //Right Paddle Horizontal Collision
    else if(ball.centerX-ball.radius>=rightPaddle.x&&ball.centerX+ball.radius<=rightPaddle.x+rightPaddle.width&&
            ball.centerY+ball.radius>=rightPaddle.y&&ball.centerY-ball.radius<=rightPaddle.y+rightPaddle.height){
        ball.velocityY=-ball.velocityY;
    }
    //Right Paddle Vericle Collision 
    else if((ball.centerY)>=rightPaddle.y&&
            (ball.centerY)<=(rightPaddle.y+rightPaddle.height)&&
            ((ball.centerX+ball.radius)>=(rightPaddle.x))){
        if(turnR){
            ball.velocityX = -ball.velocityX;
            turnL=true;
            turnR=false;            
        }
    }
}